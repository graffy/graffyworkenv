#!/usr/bin/env bash
set -o errexit

REPORTS_DIR="$HOME/work/debops/reports"
mkdir -p "${REPORTS_DIR}"
REPORT_FILE_PATH=${REPORTS_DIR}/$(date --iso=seconds)-update-debops
echo "updating debops itself"
pushd $HOME/work/debops/ansible.debops
	source ../debops.venv/bin/activate
	git pull | tee --append ${REPORT_FILE_PATH}
	./bin/update-prod.sh | tee --append ${REPORT_FILE_PATH}
	./bin/update-dev.sh | tee --append ${REPORT_FILE_PATH}
	deactivate
popd

