#!/bin/bash
# script made by graffy on 12/01/2016 to understand the diff between ./data/ipr/svnworkspaces/main and ./owncloud/svnworkspaces/main
srcPath="$1" # eg ./data/ipr/svnworkspaces/main
dstPath="$2" # eg ./ownCloud/svnworkspaces/main
bDeleteFilesFromSourceWhenIdentical="$3" # 'true' or 'false'

function deleteFile()
{
	local strFilePath="$1"
	
	local bDebug='false'
	if [ "$bDebug" = 'true' ]
	then
		echo "fake delete of $strFilePath"
	else
		echo "deleting $strFilePath"
		rm -f "$strFilePath"
	fi
}

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for srcFile in $(find "$srcPath" -type f)
do
	#echo $srcFile
	if [ $(basename "$srcFile") != '.DS_Store'  ]
	then
		dstFile=$(echo "$srcFile" | sed "s|$srcPath|$dstPath|g")
		#echo $dstFile
		if [ -f "$dstFile" ]
		then
			diff "$srcFile" "$dstFile"
			if [ $? != 0 ]
				then echo "$srcFile != $dstFile"
			else
				if [ "$bDeleteFilesFromSourceWhenIdentical" = 'true' ]
				then
					# as $srcFile and $dstFile are identical, delete srcFile
					deleteFile "$srcFile"
				fi
			fi
		else
			echo "missing file : $dstFile"
		fi
	fi
done
IFS=$SAVEIFS
