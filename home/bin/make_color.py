#!/usr/bin/env python
# this program generates a color value by hashing the input string

import sys
from colorsys import hsv_to_rgb
from hashlib import md5


def float_to_16b(component):
    # print(hex_comp)
    correction = 3. / 4.  # empiric correction, otherwise colors are not what's expected
    return int(component * 65536.0 * correction)


def float_to_8b(component):
    # print(hex_comp)
    return int(component * 256.0)


def hsv_to_osx(hue, saturation, value):
    (r, g, b) = hsv_to_rgb(hue, saturation, value)
    return "{%d, %d, %d}" % (float_to_16b(r), float_to_16b(g), float_to_16b(b))


def hsv_to_linux(hue, saturation, value):
    (r, g, b) = hsv_to_rgb(hue, saturation, value)
    return "(%d, %d, %d)" % (float_to_8b(r), float_to_8b(g), float_to_8b(b))


if __name__ == '__main__':
    seed_string = sys.argv[1]
    # print('seed_string = ', seed_string)
    color_value = float(sys.argv[2])
    color_saturation = float(sys.argv[3])
    string_format = sys.argv[4]  # eg 'osx' 'linux'
    # color_hue = int(md5(seed_string).hexdigest()[:8], 16) # taken from http://www.guguncube.com/3237/python-string-to-number-hash
    color_hue = float(int(md5(seed_string).hexdigest(), 16) % 100000) / 100000.0

    # print(color_hue)
    if string_format == 'osx':
        print(hsv_to_osx(color_hue, color_saturation, color_value))

    if string_format == 'linux':
        print(hsv_to_linux(color_hue, color_saturation, color_value))
