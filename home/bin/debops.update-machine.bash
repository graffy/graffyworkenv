#!/usr/bin/env bash
TARGET_HOST_FQDN="$1"  # the machine on which we want to apply debops

set -o errexit

REPORTS_DIR="$HOME/work/debops/reports"
mkdir -p "${REPORTS_DIR}"
REPORT_FILE_PATH=${REPORTS_DIR}/$(date --iso=seconds)-update-${TARGET_HOST_FQDN}
echo "applying debops configuration on ${TARGET_HOST_FQDN} (report stored in ${REPORT_FILE_PATH})"
pushd $HOME/work/debops/ansible.debops
	echo "ansible.debops version: $(git rev-parse HEAD)" >> ${REPORT_FILE_PATH}
	source ../debops.venv/bin/activate
	ANS_HOST=$(echo ${TARGET_HOST_FQDN} | sed -E 's/\.univ-rennes[1]?\.fr$//')
	echo "ANS_HOST=${ANS_HOST}"
	debops run site --limit "${ANS_HOST:-/dev/null}" | tee --append ${REPORT_FILE_PATH}
	deactivate
popd
