#!/usr/bin/env bash
TARGET_HOST_FQDN="$1"  # the machine on which we want to install debops bootstrap

set -o errexit

REPORTS_DIR="$HOME/work/debops/reports"
mkdir -p "${REPORTS_DIR}"
REPORT_FILE_PATH=${REPORTS_DIR}/$(date --iso=seconds)-init-${TARGET_HOST_FQDN}
echo "installing debops bootstrap on ${TARGET_HOST_FQDN} (report stored in ${REPORT_FILE_PATH})"
pushd $HOME/work/debops/ansible.debops
	source ../debops.venv/bin/activate
	ANS_HOST=$(echo ${TARGET_HOST_FQDN} | sed -E 's/\.univ-rennes[1]?\.fr$//')
	echo "ANS_HOST=${ANS_HOST}"
	debops run bootstrap-ldap -l "${ANS_HOST:-/dev/null}" | tee --append ${REPORT_FILE_PATH}
	deactivate
popd

