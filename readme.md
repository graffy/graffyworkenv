
This repository stores the work environment used by graffy at Institut de Physique de Rennes

The work environment includes:
- a `bin` directory containing custom scripts
- bash configuration files
- etc.

## how to use

clone the repository to a chosen location `local_repos_path`

```sh
graffy@graffy-ws2:~/work$ git clone git@vmgit.ipr.univ-rennes.fr:graffy/graffyworkenv.git graffyworkenv.git
```

then install the environment by creating symbolic links in your home dir that point to some files in the `local_repos_path`
```sh
graffy@graffy-ws2:~$ ln -s ./work/graffyworkenv.git/home/bin ./bin
graffy@graffy-ws2:~$ ln -s ./work/graffyworkenv.git/home/.profile ./.profile
graffy@graffy-ws2:~$ ln -s ./work/graffyworkenv.git/home/.bashrc ./.bashrc
graffy@graffy-ws2:~$ ln -s ./work/graffyworkenv.git/home/.bash_logout ./.bash_logout
```
